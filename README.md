## Cypress

### Scripts

Vizualni testy - porovnavani obrazku > https://www.npmjs.com/package/cypress-visual-regression
`component-test-base": "npx cypress run --component --browser chrome --env type=base` > vygeneruje prvni verzi obrazku
`component-test": "npx cypress run --component --browser chrome --env type=actual` > porovna uz aktualni verzi obrazku s predchozi verzi
`
Cypress testy + component testy
`cypress:open": "cypress open --env type=actual` > otevre cypress a spusti testy v 
`cypress:run": "cypress run` > otevre cypress a spusti testy v prikazove radce