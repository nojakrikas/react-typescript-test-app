/// <reference types="cypress" />

const counterSelector = '[data-test="counter"]';
const clicksSelector = '[data-test="clicks"]';
const squareSelector = '[data-test="square"]';

const getDescription = () =>
  cy.get('meta[name="description"]').should("have.attr", "content");

describe("App", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("check meta data", () => {
    cy.visit("/");
    cy.title().should("equal", "test app");
    getDescription().should("equal", "Web site created using webpack");
  });

  it("can add new square", () => {
    cy.get(counterSelector).should("have.text", "0");

    cy.get("[data-test=increaseBtn]").click();

    cy.get(counterSelector).should("have.text", "5");
  });

  it("can check count of clicks", () => {
    cy.get(clicksSelector).should("include.text", "0");

    cy.get("[data-test=increaseBtn]").click();

    cy.get(clicksSelector).should("include.text", "1");

    cy.get("[data-test=increaseBtn]").click();

    cy.get(clicksSelector).should("include.text", "2");
  });

  it("can check count of rendered items", () => {
    cy.get(squareSelector).should("have.length", 0);
    cy.get("[data-test=increaseBtn]").click();
    cy.get("[data-test=increaseBtn]").click();

    cy.get(squareSelector).should("have.length", 2);
  });
});
