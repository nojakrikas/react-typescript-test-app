import { INCREASE_COUNTER, SET_RECORDING } from '../constants/exampleConstants';
import { AppAction, AppState } from '../types/global';

const defaultState: AppState = {
  counter: 0,
  recording: false
};

export const dataReducer =
(state: AppState = defaultState, action: AppAction) => {
  switch (action.type) {
    case INCREASE_COUNTER:
      return {
        ...state,
        counter: state.counter + (action.iterator || 0)
      };
    case SET_RECORDING:
      return {
        ...state,
        recording: action.enabled
      }
    default: return state;
  }
};
