import { screen, fireEvent, render, waitFor } from "@testing-library/react";
import * as redux from "react-redux";

import { RecordTracks } from "./RecordTracks";
import { SET_RECORDING } from "../constants/exampleConstants";

// jest.useFakeTimers();
// jest.spyOn(global, "setTimeout");

const mockDispatch = jest.fn();
jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

describe("RecordTracks", () => {
  test("should dispatch setRecordings", () => {
    render(<RecordTracks />);

    const recordBtn = screen.getByTestId("recordBtn");

    fireEvent.click(recordBtn);

    expect(mockDispatch).toHaveBeenCalledWith({
      enabled: true,
      type: SET_RECORDING,
    });
  });

  test("should recording", async () => {
    jest
      .spyOn(redux, "useSelector")
      .mockImplementation((selectorFn) =>
        selectorFn({ data: { recording: true } })
      );

    render(<RecordTracks />);

    const seconds = screen.getByTestId("sec");

    // expect(setTimeout).toHaveBeenCalledTimes(1);
    // expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000);

    await waitFor(() => screen.findByText("1 s"), { timeout: 1500 });

    expect(seconds.innerHTML).toContain("1 s");
  });
});
