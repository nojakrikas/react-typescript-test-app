import { getRandomColor } from "../functions";
import { CenterPanel, Square } from "./Layout";

export const Content = ({ clicks }: { clicks: number }) => (
  <CenterPanel>
    {Array.from(Array(clicks).keys()).map((id) => (
      <Square key={`key_${id}`} id={id} color={getRandomColor()} />
    ))}
  </CenterPanel>
);
