import { render, screen } from "@testing-library/react";

import { rest } from "msw";
import { setupServer } from "msw/node";

import { SquareAsync } from "./SquareAsync";

// https://mswjs.io/docs/
const server = setupServer(
  // Describe network behavior with request handlers.
  // Tip: move the handlers into their own module and
  // import it across your browser and Node.js setups!
  rest.get("https://www.thecolorapi.com/id", (_req, res: any, ctx: any) => {
    return res(
      ctx.json({
        name: {
          closest_named_hex: "#004816",
          distance: 36,
          exact_match_name: false,
          value: "Crusoe",
        },
      })
    );
  })
);

const color = "rgb(0, 0, 0)";

// Enable request interception.
beforeAll(() => server.listen());

// Reset handlers so that each test could alter them
// without affecting other, unrelated tests.
afterEach(() => server.resetHandlers());

// Don't forget to clean up afterwards.
afterAll(() => server.close());

describe("SquareAsync", () => {
  test("should match snapshot", async () => {
    render(<SquareAsync id={1} color={color} />);

    const loading = await screen.findByText("loading...");
    expect(loading).toBeInTheDocument();

    const colorName = await screen.findByTestId("Crusoe");
    expect(colorName).toBeInTheDocument();
    expect(colorName.innerHTML).toContain("Crusoe");
  });
});
