import React, { useState } from "react";

export const CenterPanel = ({ children }: { children: React.ReactNode }) => (
  <div
    style={{
      margin: "0 auto",
      width: 500,
      textAlign: "center",
      marginBottom: 20,
    }}
  >
    {children}
  </div>
);

export const Square = ({ id, color }: { id: number; color: string }) => {
  const [SHOW, setVisible] = useState(false);

  return (
    <div
      style={{
        width: 50,
        height: 50,
        display: "inline-block",
        float: "left",
        backgroundColor: color,
      }}
      onClick={() => setVisible(!SHOW)}
      data-test="square"
      data-testid={id}
    >
      <span style={{ position: "relative", top: "calc(50% - .5rem)" }}>
        {SHOW ? id + 1 : ""}
      </span>
    </div>
  );
};

export function getSquare(id: number, color: string) {
  const div = document.createElement("div");
  div.className = "test";
  div.style.width = "50px";
  div.style.height = "50px";
  div.style.display = "inline-block";
  div.style.float = "left";
  div.style.backgroundColor = color;

  const main = document.getElementById("main");
  main?.appendChild(div);
}
