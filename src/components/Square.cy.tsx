import { mount } from "cypress/react18";

import { Square } from "./Layout";

describe("Square", () => {
  it("should match snapshots", () => {
    cy.viewport("iphone-se2");

    mount(<Square id={1} color="rgb(1, 120, 0)" />);

    cy.compareSnapshot("test-1", 0.1);
  });
});
