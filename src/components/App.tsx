import { useEffect, useState } from "react";
import { ThemeProvider } from "@mui/material/styles";

import { createGlobalStyle } from "styled-components";
import { Button, Typography, Paper } from "@mui/material";
import { getTheme, MODE } from "../themes";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { increaseCounter } from "../actions/actions";
import { GlobalState } from "../types/global";
import { CenterPanel } from "./Layout";
import { RecordTracks } from "./RecordTracks";
import { Content } from "./Content";
import { SquareAsync } from "./SquareAsync";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }
`;

export const App = () => {
  const [CLICKS, increaseClick] = useState<number>(0);
  const COUNTER = useSelector((state: GlobalState) => state.data.counter);
  const dispatch = useDispatch<ThunkDispatch<{}, {}, AnyAction>>();
  const [MODE_TYPE, setMode] = useState(MODE.LIGHT);

  useEffect(() => {
    increaseClick(0);
  }, []);

  useEffect(() => {
    if (CLICKS % 2 === 0) {
      setMode(MODE.DARK);
    } else {
      setMode(MODE.LIGHT);
    }
  }, [CLICKS]);

  return (
    <ThemeProvider theme={getTheme(MODE_TYPE)}>
      <GlobalStyle />
      <Paper style={{ width: "100%", height: "100%" }} id="main">
        <CenterPanel>
          <RecordTracks />
          <Typography variant="h2" data-test="counter">
            {COUNTER}
          </Typography>
          <Typography
            variant="h6"
            data-test="clicks"
          >{`počet kliknutí je ${CLICKS}`}</Typography>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              dispatch(increaseCounter(5));
              increaseClick(CLICKS + 1);
            }}
            data-test="increaseBtn"
          >
            Increase
          </Button>
        </CenterPanel>
        <Content clicks={CLICKS} />
        <SquareAsync id={1} color="rgb(0,0,0)"/>
      </Paper>
    </ThemeProvider>
  );
};
