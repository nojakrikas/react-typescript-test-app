import { useCallback, useEffect, useState } from "react";

import { Button, Typography } from "@mui/material";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { AnyAction, Dispatch } from "redux";
import { toggleRecording } from "../actions/actions";
import { GlobalState } from "../types/global";
import { Stop, FiberManualRecord as Rec } from "@mui/icons-material";

export const RecordTracks = () => {
  const RECORDING = useSelector((state: GlobalState) => state.data.recording);
  const [SEC, setSeconds] = useState(0);

  const doTick = useCallback(
    () =>
      setTimeout(() => {
        setSeconds(SEC + 1);
      }, 1000),
    [SEC]
  );

  useEffect(() => {
    if (RECORDING) {
      doTick();
    } else {
      setSeconds(0);
    }
  }, [RECORDING, SEC, doTick]);

  const dispatch = useDispatch<Dispatch<AnyAction>>();

  return (
    <>
      <Button
        disabled={RECORDING}
        startIcon={
          !RECORDING ? (
            <Rec style={{ color: RECORDING ? "inherit" : "red" }} />
          ) : (
            <div
              style={{
                width: 15,
                height: 15,
                borderRadius: 10,
                margin: 2.5,
                display: "block",
                backgroundColor: "red",
                animation: "pulse 2s infinite",
              }}
            />
          )
        }
        color="primary"
        style={{ width: "50%", height: 40, textTransform: "initial" }}
        onClick={() => dispatch(toggleRecording(true))}
        data-testid="recordBtn"
      >
        <Typography style={{ textTransform: "initial" }} data-testid="sec">
          {!RECORDING ? "" : `${SEC} s`}
        </Typography>
      </Button>

      <Button
        disabled={!RECORDING}
        color="primary"
        style={{ width: "50%", height: 40 }}
        onClick={() => dispatch(toggleRecording(false))}
      >
        <Stop style={{ color: !RECORDING ? "inherit" : "red" }} />
      </Button>
    </>
  );
};
