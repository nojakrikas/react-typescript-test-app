import { combineReducers } from 'redux';
import { dataReducer } from './reducers/app-reducer';

export declare type partialReducer = (partialStore: object) => object;

export const createAppReducer = () => {
  return combineReducers({
    data: dataReducer
  });
};
