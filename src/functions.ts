const gRC = () => Math.floor(Math.random() * 255);

export const getRandomColor = () => `rgb(${gRC()}, ${gRC()}, ${gRC()})`;
