import React from "react";
import { createRoot } from 'react-dom/client';
import { Provider } from "react-redux";

import { configureStore } from "./store";
import "./index.css";
import { App } from "./components/App";
import reportWebVitals from "./reportWebVitals";

const store = configureStore();

const render = (Component: () => JSX.Element) => {

  const container = document.querySelector('.root') as HTMLElement;
  const root = createRoot(container);
  root.render(
    <React.StrictMode>
    <Provider store={store}>
      <Component />
    </Provider>
    </React.StrictMode>
  );
};

render(App);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
