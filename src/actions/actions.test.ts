import { INCREASE_COUNTER, SET_RECORDING } from "../constants/exampleConstants";
import { increaseCounter, setRecording } from "./actions";

const dispatch = jest.fn();

describe("actions", () => {
  test("increaseCounter", () => {
    expect(increaseCounter(1)).toEqual({
      iterator: 1,
      type: INCREASE_COUNTER,
    });
  });

  test("setRecording", () => {
    setRecording(true)(dispatch);

    expect(dispatch.mock.calls.length).toBe(1);
    expect(dispatch.mock.calls[0]).toEqual([
      {
        type: SET_RECORDING,
        enabled: true,
      },
    ]);
  });
});

//   const dispatch = jest.fn();
//   actions.yourAction()(dispatch);

//   expect(dispatch.mock.calls.length).toBe(1);

//   expect(dispatch.mock.calls[0]).toBe({
//     type: SET_DATA,
//     value: {...},
//   });
