import { Dispatch } from "redux";
import { INCREASE_COUNTER, SET_RECORDING } from "../constants/exampleConstants";

export const increaseCounter = (iterator: number) => ({
  iterator,
  type: INCREASE_COUNTER,
});

export const setRecording = (enabled: boolean) => (dispatch: Dispatch) => {
  dispatch({
    enabled,
    type: SET_RECORDING,
  });
};

export const toggleRecording = (enabled: boolean) => ({
  enabled,
  type: SET_RECORDING,
});
