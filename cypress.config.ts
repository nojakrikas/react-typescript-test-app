import { defineConfig } from "cypress";
import getCompareSnapshotsPlugin from "cypress-visual-regression/dist/plugin";

const webpackConfig = require('./webpack/config.development.js');

export default defineConfig({
  e2e: {
    screenshotsFolder: "./cypress/snapshots/actual",
    trashAssetsBeforeRuns: true,
    video: false,
    baseUrl: "http://localhost:8080",
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },

  component: {
    specPattern: 'src/**/*.cy.{js,jsx,ts,tsx}',
    devServer: {
      framework: "react",
      bundler: "webpack",
      webpackConfig: webpackConfig
    },
    setupNodeEvents(on, config) {
      getCompareSnapshotsPlugin(on, config);

      on("before:browser:launch", (browser, launchOptions) => {
        if (browser.family === "chromium") {
          launchOptions.args.push("--hide-scrollbars");
        }

        return launchOptions;
      });
    },
  },
});
